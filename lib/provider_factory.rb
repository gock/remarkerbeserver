class ProviderFactory
  def self.choose provider
    require "provider/#{provider}"
    Object.const_get "#{provider.capitalize}Provider"
  end
end
