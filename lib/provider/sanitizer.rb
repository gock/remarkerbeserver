require 'action_controller/vendor/html-scanner'
# See http://dev.evernote.com/documentation/cloud/chapters/ENML.php
# https://github.com/rails/rails/blob/1bd88fdafd9cd0c1c83e8bf41020d0c251cb3108/actionpack/lib/action_controller/vendor/html-scanner/html/sanitizer.rb#L108
class EvernoteSanitizer < HTML::WhiteListSanitizer
  self.bad_tags =  %w(applet base basefont bgsound blink body button dir embed fieldset form frame frameset head html iframe ilayer input isindex label layer, legend link marquee menu meta noframes noscript object optgroup option param plaintext script select style textarea xml)
  self.allowed_attributes = allowed_attributes + %w(style) - %w(id class onclick ondblclick accesskey data dynsrc tabindex)
  self.allowed_tags = %w(a abbr acronym address area b bdo big blockquote br caption center cite code col colgroup dd del dfn div dl dt em font h1 h2 h3 h4 h5 h6 hr i img ins kbd li map ol p pre q s samp small span strike strong sub sup table tbody td tfoot th thead title tr tt u ul var xmp)
  self.allowed_protocols = %w(http https file)
  self.allowed_css_properties += %w(background border margin padding list-style border-radius)
 end
Evernote::EDAM::Type::Note.instance_eval do
  @sanitizer = EvernoteSanitizer.new
  @self_closing_tags = %w(area br col command hr img keygen source  wbr)
  @regex_self_closing_tag = /<(#{@self_closing_tags.join "|"})\b([^>]*?)(?<!\/)>/

  def self.sanitize_content c
    puts @regex_self_closing_tag 
    @sanitizer.sanitize(c).gsub @regex_self_closing_tag, '<\1 \2 />'
  end
end

