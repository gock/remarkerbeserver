require 'provider/note_provider'
class EvernoteProvider < NoteProvider
  def self.get_client options
      client = EvernoteOAuth::Client.new( :token => options[:authtoken])
  end
  def self.get_user options
    client = get_client options
    user = client.user_store.getUser
    JSON::load user.to_json
  end
  def self.transcode str
    str.force_encoding "ascii-8bit"
  end
  def self.sync options
    begin
      client = get_client options
      note_store = client.note_store
      # see http://dev.evernote.com/documentation/reference/Types.html#Struct_Tag
      tags = ((options[:tags] || []) + ['remarkerbe']).collect{ |i| i.gsub(/,/, "").squish[0..98].squish.capitalize}.uniq
      note = Evernote::EDAM::Type::Note.new(
        :title => transcode((options[:title].present? && options[:title] || "No Title").squish.gsub(/\p{Cc}\p{Z}/u,''))[0..254],
        :tagNames => tags.collect{ |k| transcode k}
      )
      note.attributes = Evernote::EDAM::Type::NoteAttributes.new(
        sourceURL: options[:url]
      )
    rescue Exception => e
      raise AuthorizationException, ['pre_save', e.message, e.inspect].join("\n"), e.backtrace
    end
    begin
      content = Evernote::EDAM::Type::Note.sanitize_content(options[:content] || "")
      note.content = transcode "<?xml version='1.0' encoding='UTF-8'?><!DOCTYPE en-note SYSTEM 'http://xml.evernote.com/pub/enml2.dtd'> <en-note>#{content}<br/> </en-note>"
      replace_image note
      note_store.createNote(note)
    rescue Evernote::EDAM::Error::EDAMUserException => e
      raise SavingNoteException, [e.message, e.inspect].join("\n"), e.backtrace
    rescue Exception => e
      msg = [e.message, e.inspect].join("\n")
      #raise AuthorizationException,  [e.message, e.inspect].join("\n"), e.backtrace if [e.message, e.inspect].join("\n").match("INVALID_AUTH")
      raise SavingNoteException, msg, e.backtrace if msg.match 'UTF-8'
      raise AuthorizationException, msg, e.backtrace
    end
  end
  def self.replace_image note
    note.content = note.content.gsub(/<img.*?src=(\"|\')(.*?)(\"|\').*?\/>/) do |s|
      begin
      file = open($2)
      hexdigest = note.add_resource(file.hash.to_s, file.read, file.content_type)
      "<en-media type=\"#{file.content_type}\" hash=\"#{hexdigest}\"/>"
      rescue Exception => e
        raise e
      end
    end
  end
end
