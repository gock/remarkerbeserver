class UploadController < ApplicationController
$pdf_error_logger = Logger.new(Rails.root + "log/pdf_error.log")  
$pdf_error_logger.formatter = proc { |severity, datetime, progname, msg|
    "#{datetime}: #{msg}\n"
  }
  def index
     render 'upload_file.erb',  :layout => false
  end
  def upload
    begin
      logger.info params
      file = begin 
               if params[:upload].present?
                 DataFile.save(params[:upload])
               elsif params[:url].present?
                 DataFile.download(params[:url])
               end
             end
      #render :text => "File has been uploaded successfully"
      redirect_to 'http://file.remarker.be/' + file;
    rescue ArgumentError => e
      render :text => e.message
    rescue Exception => e
      render :inline => "<pre><h2>Unhandled Error</h2>\nmessage: #{e.message || "no"}</pre>"
    ensure
      return if e.blank?
      $pdf_error_logger.error "===================================================="
      $pdf_error_logger.error params
      $pdf_error_logger.error e.inspect
      $pdf_error_logger.error e.message
      $pdf_error_logger.error e.backtrace[0..5]
    end
  end
end
