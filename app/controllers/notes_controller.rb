# encoding: utf-8
#Encoding::default_internal = "utf-8"
#Encoding::default_external = "utf-8"
require 'encryptor'
require 'provider_factory.rb'
require 'provider/note_error'
$note_error_logger = Logger.new(Rails.root + "log/note_sync_error.log")  
$note_error_logger.formatter = proc { |severity, datetime, progname, msg|
    "#{datetime}: #{msg}\n"
  }
class NotesController < ApplicationController
  rescue_from Exception, :with => :log_error
  rescue_from AuthorizationException, :with => :process_authorize
  rescue_from SavingNoteException, :with => :process_saving_exception

  before_filter :get_authtoken_from_cookie, :only => :sync
  before_filter :process_params, :only => :sync
  before_filter :authenticate_user!, :only =>:show

  # POST /notes/sync
  def sync
    logger.info cookies.to_a
    logger.info session
    logger.info request.env['HTTP_USER_AGENT']
    @note = Note.create params[:note] 
    session[:note_id]= @note.id
    cookies[:note_id] = { :value => @note.id, :expires => 1.days.from_now.utc }
    logger.info @note.id
    raise AuthorizationException, "Not found cookie.Need init." if @authtoken.blank?
    # todo : wrong combination of token and provider
    @provider = cookies[:provider]
    callback_sync
  end

  def callback_sync
    logger.info cookies.to_a
    logger.info session
    logger.info params[:pid]
    @authtoken ||= request.env['omniauth.auth']['credentials']['token']
    @provider ||= (params[:endpoint] || "evernote")
    @is_cn = /zh-cn/i.match request.env["HTTP_ACCEPT_LANGUAGE"]
    logger.info @provider
    logger.info @authtoken
    cookies[:rmkb_token] = { :value => encrypt_authtoken(@authtoken), :expires => 1.years.from_now.utc }
    cookies[:provider] = { :value => @provider, :expires => 1.years.from_now.utc }
    note_id = session[:note_id] || cookies[:note_id] || params[:pid]
    logger.info note_id
    note = Note.find note_id
    create_note note
    save_user_info note
    reset_session
    render "_success"
  end

  def show
    @note = Note.find params[:id]
  end

  def logout 
    cookies.delete :rmkb_token
    cookies.delete :provider 
    render '_logout', :layout => false
  end

  def auth_fail
    msg = params[:message] || 'Session expired. Please try again.'
    msg = "Error! #{msg}"
    render :text => msg
  end
  private
  def  get_authtoken_from_cookie
    @authtoken = cookies[:rmkb_token]
    @authtoken = decrypt_authtoken @authtoken if @authtoken.present?
  end
  def process_params
    params[:note][:content] = CGI::unescapeHTML(params[:note][:content])
    params[:note][:tags] = JSON::load params[:note][:tags]
    params[:note][:words] = JSON::load params[:note][:words]
    params[:note][:provider] = "evernote"
  end
  def secret_key
    "$@#!%U^H%D#1&Pw&**".freeze
  end
  def encrypt_authtoken str
    Base64::encode64 Encryptor.encrypt(str, :key => secret_key)
  end
  def decrypt_authtoken str
    Encryptor.decrypt(Base64::decode64(str), :key => secret_key)
  end
  def save_user_info note
    info =  provider.get_user :authtoken => @authtoken
    note.username = info["username"]
    note.provider = @provider
    note.save
  end
  def create_note note
    provider.sync :authtoken => @authtoken, :content => note.content, :title => note.title, :url => note.url, :tags => note.tags
  end
  
  def provider
    ProviderFactory.choose(@provider || "evernote")
  end
  def process_authorize e
    log_error e, false
    cookies.delete :rmkb_token
    cookies.delete :provider 
    @message = { :status => 302, :url => "http://#{request.host_with_port}/auth/evernote", :base => "http://#{request.host_with_port}/auth/" }
    #@message = { :status => 302, :url => "http://#{request.host_with_port}/auth/evernote"}
    #@message = { :status => 302, :url => "http://#{request.host_with_port}/auth/yinxiang"}
    @is_cn = /zh-cn/i.match request.env["HTTP_ACCEPT_LANGUAGE"]
    render "_redirect", :layout => false
  end
  def process_saving_exception e
    log_error e, false
    render "_fail", :layout => false
  end
  def log_error e, raise_again = true
    $note_error_logger.error "===================================================="
    $note_error_logger.error session[:note_id]
    $note_error_logger.error e.inspect
    $note_error_logger.error e.message
    $note_error_logger.error e.backtrace[0..5]
    logger.error e.inspect
    logger.error e.message
    logger.error e.backtrace
    if raise_again
      render "_fail", :layout => false
    end
  end
  

end
