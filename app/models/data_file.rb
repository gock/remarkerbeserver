require 'digest/md5'
require 'open-uri'
class DataFile 
  def self.logger 
    ActiveRecord::Base.logger 
  end
  def self.save(upload)
    raise  ArgumentError, "Not valid PDF file" if upload['datafile'].content_type != 'application/pdf'
    name =  upload['datafile'].original_filename
    directory = create_path_by_name name 
    
    # create the file path
    path = File.join(directory, name)
    # write the file
    File.open(path, "wb") { |f| f.write(upload['datafile'].read) }

    process_pdf path, directory
  end
  
  def self.download url
    file = open(url)
    raise  ArgumentError, "Exceed max file size" if file.meta['content-length'].to_i > @@file_size_limit
    raise ArgumentError, "Not valid PDF file" if file.content_type != 'application/pdf'
    name = Digest::MD5.hexdigest url
    directory = create_path_by_name name 

    path = File.join(directory, name)
    File.open(path, "wb") { |f| f.write(file.read) }

    process_pdf path, directory
  end
  
  def self.process_pdf path, directory
    cmd = "pdf2htmlEX --zoom 1.5 --embed cfijo '#{path}' --dest-dir #{directory} --data-dir public/pdfconfig  2>&1"
    logger.info cmd
    #logger.info system(cmd)
    '''
    pipe = IO.popen(cmd)
    output = []
    while (line = pipe.gets)
      output.push line
    end
    '''
    output = %x[#{cmd}].inspect

    logger.info output

    #js
    #js_path = File.join(directory, 'pdf2htmlEX.js')
    #File.open(js_path, 'a') { |f| f.puts @@inject_js }

    #html
    output_file =  Dir[directory + '/*.html'][0]
    raise "Process failure!\n#{output}" if output_file.blank?
    html_file = output_file.gsub "public/data/", ''
  end

  def self.create_path_by_name name
    folder = Digest::MD5.hexdigest name
    d = 0.days.from_now.to_date.to_s
    directory = "public/data/" + d
    Dir.mkdir directory unless Dir.exist?(directory)  
    directory = directory + '/' +folder
    Dir.mkdir directory unless Dir.exist?(directory) 
    directory
  end

  @@file_size_limit = 5 * 1024 * 1024;
  @@inject_js = <<-CODE
  javascript:(function(){
    var createElement = function (tag,attrs){
    var elem = document.createElement(tag);
    for (var key in attrs){
      elem.setAttribute(key, attrs[key]);
    };
    return elem;
  };
  var loadJS = function(src, success) {
    var domScript = createElement('script', { 'src' : src, 'type' : 'text/javascript'});
    success = success || function(){};
    domScript.onload = domScript.onreadystatechange = function() {
      if (!this.readyState || 'loaded' === this.readyState || 'complete' === this.readyState) {
      success();
      this.onload = this.onreadystatechange = null;
      this.parentNode.removeChild(this);
    }
    }
    document.body.appendChild(domScript);
  };
  $(document).ready(function(){
  loadJS('http://remarker.be/js/markalbe.js');
  })
  }());
  CODE
end
