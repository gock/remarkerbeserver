class Note < ActiveRecord::Base
  attr_accessible :content, :url, :user_id, :title, :provider, :tags, :words, :username
  serialize :tags
  serialize :words
end
