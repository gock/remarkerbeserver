config = YAML.load(ERB.new(File.read(Rails.root.join('config/evernote.yml'))).result)[Rails.env]
site = config['sandbox'] ? 'https://sandbox.evernote.com' : 'https://www.evernote.com'
yinxiang = 'https://app.yinxiang.com/'

Rails.application.config.middleware.use OmniAuth::Builder do
  #provider :evernote, config['consumer_key'], config['consumer_secret'], :client_options => {:site => site}
  #provider :evernote, config['consumer_key'], config['consumer_secret'], :client_options => {:site => yinxiang}
  provider :evernote, config['consumer_key'], config['consumer_secret'], :setup => lambda{|env|  env['omniauth.strategy'].options[:client_options][:site] = /endpoint=yinxiang/.match(env["QUERY_STRING"].to_s) ? yinxiang : site;puts env["omniauth.strategy"].options; puts env["QUERY_STRING"]} 
  #provider :sandbox, config['consumer_key'], config['consumer_secret'], :client_options => {:site => 'https://sandbox.evernote.com' }
end


OmniAuth.config.on_failure do |env|
  exception = env['omniauth.error']
  error_type = env['omniauth.error.type']
  strategy = env['omniauth.error.strategy']

  Rails.logger.error("OmniAuth Error (#{error_type}): #{exception.inspect}")
  #ErrorNotifier.exception(exception, :strategy => strategy.inspect, :error_type => error_type)

  new_path = "#{env['SCRIPT_NAME']}#{OmniAuth.config.path_prefix}/failure?message=#{error_type}"

  [302, {'Location' => new_path, 'Content-Type'=> 'text/html'}, []]
end
#OmniAuth.config.on_failure = LoginController.action(:oauth_failure)
