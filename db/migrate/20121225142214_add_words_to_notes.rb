class AddWordsToNotes < ActiveRecord::Migration
  def change
    add_column :notes, :words, :text
  end
end
