class AlertNoteContent< ActiveRecord::Migration
  def self.up
    # this bumps col from TEXT to MEDIUMTEXT in mysql
    change_column :notes, :content, :text, :limit => 256.kilobytes + 1
  end

  def self.down
    change_column :notes, :content, :text
  end
end
