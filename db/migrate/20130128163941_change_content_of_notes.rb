class ChangeContentOfNotes < ActiveRecord::Migration
  def up
    change_column :notes, :content, :text, :limit => 256.kilobytes + 1
  end

  def down
    change_column :notes, :content, :text
  end
end
