class AddProviderToNotes < ActiveRecord::Migration
  def change
    add_column :notes, :provider, :string
  end
end
