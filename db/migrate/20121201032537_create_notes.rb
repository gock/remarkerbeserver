class CreateNotes < ActiveRecord::Migration
  def change
    create_table :notes do |t|
      t.integer :user_id
      t.text :url
      t.text :content

      t.timestamps
    end
  end
end
